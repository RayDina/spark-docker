FROM raydina/hadoop:2.7.4
MAINTAINER raydina

#scala 2.11.11
RUN rpm --rebuilddb && yum install -y https://downloads.lightbend.com/scala/2.11.11/scala-2.11.11.rpm --nogpgcheck
ENV SCALA_HOME /usr/share/scala

# Spark 2.2 for Hadoop 2.7.0
RUN curl -s http://www.apache.org/dist/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz | tar -xz -C /opt
RUN ln -s /opt/spark-2.2.0-bin-hadoop2.7 /usr/local/spark && chown root:root /usr/local/spark/ -R
ENV SPARK_HOME /usr/local/spark
RUN mkdir $SPARK_HOME/yarn-remote-client
ADD yarn-remote-client $SPARK_HOME/yarn-remote-client

RUN $BOOTSTRAP && $HADOOP_PREFIX/bin/hadoop dfsadmin -safemode leave && $HADOOP_PREFIX/bin/hdfs dfs -put $SPARK_HOME/jars /spark && $HADOOP_PREFIX/bin/hdfs dfs -put $SPARK_HOME/examples/jars /spark

ENV YARN_CONF_DIR $HADOOP_PREFIX/etc/hadoop
ENV PATH $PATH:$SPARK_HOME/bin:$HADOOP_PREFIX/bin
# update boot script
COPY bootstrap.sh /etc/bootstrap.sh
RUN chown root.root /etc/bootstrap.sh
RUN chmod 700 /etc/bootstrap.sh

ENTRYPOINT ["/etc/bootstrap.sh"]
